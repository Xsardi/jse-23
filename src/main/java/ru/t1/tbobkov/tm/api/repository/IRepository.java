package ru.t1.tbobkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {
            @Override
            @NotNull
            public Optional<M> findOneById(@Nullable String id) {
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }

            @Override
            @NotNull
            public Optional<M> findOneByIndex(@Nullable Integer index) {
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }
        };
    }

    interface IRepositoryOptional<M> {
        @NotNull
        Optional<M> findOneById(@Nullable String id);

        @NotNull
        Optional<M> findOneByIndex(@Nullable Integer index);

    }

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @NotNull
    M add(@NotNull M model);

    boolean existsById(@Nullable String id);

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    int getSize();

    @Nullable
    M remove(@Nullable M model);

    @Nullable
    M removeById(@Nullable String id);

    @Nullable
    M removeByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<M> collection);

}
