package ru.t1.tbobkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull String email);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable Role role);

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User removeByEmail(@NotNull String email);

    @Nullable
    User removeByLogin(@NotNull String login);

    @NotNull
    User setPassword(@NotNull String id, @NotNull String password);

    @NotNull
    User update(@NotNull String id, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName);

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    @NotNull
    Boolean isEmailExist(@NotNull String email);

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

}
