package ru.t1.tbobkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void removeProjectById(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Task unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

}
